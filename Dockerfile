FROM fingerprintsoft/java
MAINTAINER Fuzail Sarang <fuzail@fingerprintsoft.co>

ENV MAVEN_VERSION="3.3.9" \
    M2_HOME=/usr/share/maven

RUN cd /tmp && \
  wget "http://ftp.unicamp.br/pub/apache/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz" && \
  tar -zxvf "apache-maven-$MAVEN_VERSION-bin.tar.gz" && \
  mv "apache-maven-$MAVEN_VERSION" "$M2_HOME" && \
  ln -s "$M2_HOME/bin/mvn" /usr/bin/mvn && \
  rm -rf /tmp/* 
  

ENV MAVEN_HOME /usr/share/maven

CMD ["mvn", "--version"]
