MAJOR ?= 3
MINOR ?= 3
BUILD ?= 9
.PHONY: all

all: build push

build:
	docker build --rm -t fingerprintsoft/maven .
	docker tag fingerprintsoft/maven fingerprintsoft/maven:${MAJOR}.${MINOR}.${BUILD}

push:
	docker push fingerprintsoft/maven
	docker push fingerprintsoft/maven:${MAJOR}.${MINOR}.${BUILD}
